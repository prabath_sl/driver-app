import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { MainProvider } from '../main/main';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the JourneyProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class JourneyProvider {

  constructor(public http: HttpClient, private mainProvider: MainProvider) {
    console.log('Hello JourneyProvider Provider');
  }
  getBusDetailsbyID(bus): Observable<any> {
    return this.mainProvider.post('/bus/findByBusID', bus)
      .map(
        data => {
          return data;
        }
      )
      .catch(res => {
        return Observable.throw(res);
      });
  }
}
