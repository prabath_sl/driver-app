import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { config } from '../../config/config';
import { JwtProvider } from '../jwt/jwt';

/*
  Generated class for the MainProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MainProvider {

  constructor(public http: HttpClient, private jwtProvider: JwtProvider) {
    console.log('Hello MainProvider Provider');
  }

  //Setting Headers for API Request
  private setHeaders(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (this.jwtProvider.getToken()) {
      headersConfig['Authorization'] = `Token ${this.jwtProvider.getToken()}`;
    }
    return new HttpHeaders(headersConfig);
  }

  //Perform POST Request
  post(path, body): Observable<any> {
    return this.http.post(
      `${config.api_url}${path}`,
      JSON.stringify(body),
      { headers: this.setHeaders() }
    )
      .catch(res => {
        return Observable.throw(res);
      })
      .map((res) => res);
  }
}
