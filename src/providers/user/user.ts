import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { MainProvider } from '../main/main';
import { Observable } from 'rxjs/Observable';
import { JwtProvider } from '../jwt/jwt';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {

  constructor(public http: HttpClient, private mainProvider: MainProvider, private jwtService: JwtProvider) {
    console.log('Hello UserProvider Provider');
  }
  userLogin(user): Observable<any> {
    this.destroyAuth()
    return this.mainProvider.post('/user/login', user)
      .map(
        data => {
          this.setAuth(data);
          return data;
        }
      )
      .catch(res => {
        return Observable.throw(res);
      });
  }
  //Store Authorization Information
  setAuth(user) {
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(user.token.token);
  }

  destroyAuth() {
    // Remove JWT from localstorage
    this.jwtService.destroyToken();
  }

  /**
   * get user by id 
   * @param user 
   */
  getUserByID(user): Observable<any> {
    return this.mainProvider.post('/user/getUser', user)
      .map(
        data => {
          return data;
        }
      )
      .catch(res => {
        return Observable.throw(res);
      });
  }
}
