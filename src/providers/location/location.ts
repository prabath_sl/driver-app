import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { MainProvider } from '../main/main';
import { Observable } from 'rxjs/Observable';
import { JwtProvider } from '../jwt/jwt';

/*
  Generated class for the LocationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationProvider {

  constructor(public http: HttpClient, private mainProvider: MainProvider, private jwtService: JwtProvider) {
    console.log('Hello LocationProvider Provider');
  }
  /**
   * get user by id 
   * @param user 
   */
  emergency(data): Observable<any> {
    return this.mainProvider.post('/emergency/new', data)
      .map(
        data => {
          return data;
        }
      )
      .catch(res => {
        return Observable.throw(res);
      });
  }
}
