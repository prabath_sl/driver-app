import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../../config/config'
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';

/*
  Generated class for the LocationDistributorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocationDistributorProvider {
  private url = config.Socket_URL;
  private socket;

  constructor(public http: HttpClient) {

  }

  sendLocation(message) {
    console.log(message)
    this.socket.emit('location', message);
  }
  SendUserID(user) {
    this.socket = io(this.url);
    console.log(this.socket)
    this.socket.emit('getUser', user);
  }
  getLocations() {
    console.log(this.socket)
    let observable = new Observable(observer => {
      this.socket.on('trackers', (data) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    })
    return observable;
  }

  // ConnectionStablish(user) {
  //   this.socket = io(this.url);
  //   console.log(this.socket)
  //   let observable = new Observable(observer => {
  //     this.socket.emit('setUser', user)
  //     return () => {
  //       this.socket.disconnect();
  //     };
  //   })
  //   return observable;
  // }
}
