import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, ToastController, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../home/home';
import { BusProvider } from '../../providers/bus/bus';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user: any = {
    email: '',
    password: ''
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public menu: MenuController,
    public toastCtrl: ToastController, private userProvider: UserProvider, public forgotCtrl: AlertController
    , private busProvider: BusProvider) {
    this.menu.swipeEnable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  login() {
    this.userProvider.userLogin(this.user)
      .subscribe((data) => {
        if (data.data.role == "Driver" || data.data.role == 1) {
          let toast = this.toastCtrl.create({
            message: 'Welcome to the iBus',
            duration: 2000,
            position: 'bottom'
          });
          toast.present();
          console.log(data.data)
          this.busProvider.getBusDetailsbyID({ bus_id: data.data.bus_id._id })
            .subscribe((bus) => {
              window.localStorage['user_id'] = data.data._id;
              window.localStorage['bus_id'] = data.data.bus_id._id;
              window.localStorage['email'] = data.data.email;
              window.localStorage['total_km'] = bus.data.routeNo.total_km;
              window.localStorage['route_id'] = bus.data.routeNo._id;
              window.localStorage['allocated_time'] = bus.data.routeNo.allocated_time;
              console.log(bus);
              this.navCtrl.setRoot(HomePage);
            }, (error) => {
              const alert = this.forgotCtrl.create({
                title: 'Error!',
                subTitle: "Invalid bus details please contact Administrator",
                buttons: ['OK']
              });
              alert.present();
            })
        } else {
          const alert = this.forgotCtrl.create({
            title: 'Error!',
            subTitle: "Invalid User please contact Administrator",
            buttons: ['OK']
          });
          alert.present();
        }

      }, (error) => {
        const alert = this.forgotCtrl.create({
          title: 'Error!',
          subTitle: error.error.msg,
          buttons: ['OK']
        });
        alert.present();
      })
  }

  forgotPass() {
    let forgot = this.forgotCtrl.create({
      title: 'Forgot Password?',
      message: "Enter you email address to send a reset link password.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
          }
        },
        {
          text: 'Send',
          handler: data => {
            let toast = this.toastCtrl.create({
              message: 'Email send successfully',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
