import { Pipe, PipeTransform } from '@angular/core';
import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { mapConfig } from '../../config/mapConfig';
import { LocationDistributorProvider } from '../../providers/location-distributor/location-distributor';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/timer'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import { LocationProvider } from '../../providers/location/location';


declare var google;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('map') mapElement: ElementRef;
  isStarted: boolean = false;
  currentLocation: any;
  map: any;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  watch: any;
  markerBusImage: String = "assets/img/busmarker.png"
  markers: any = [];
  displayValues: any;
  totalTime: any = window.localStorage['allocated_time'];
  locations: any;
  timer: any;
  countDown;
  counter;
  tick = 1000;

  constructor(private alertCtrl: AlertController, public navCtrl: NavController, private geolocation: Geolocation,
    private locationDistributor: LocationDistributorProvider, public emergencyService: LocationProvider) {
    // this.showRadio();
    this.locationDistributor.SendUserID({ id: window.localStorage['user_id'] })
    // this.validateJourney();
    console.log(this.counter)
  }

  ngOnDestroy() {


    if (this.locations != null && this.locations != undefined)
      this.locations.unsubscribe();

    if (this.watch != null && this.watch != undefined)
      this.watch.unsubscribe();

    if (this.timer != null && this.timer != undefined)
      this.timer.unsubscribe();
  }

  /**
   * Distribut location object
   * @param locationObject 
   */
  sendLocation(locationObject) {
    this.locationDistributor.sendLocation(locationObject);
  }

  /**
   * check weather the journey is already started or not
   */
  // validateJourney() {
  //   if (window.localStorage.getItem("journey") != null && window.localStorage.getItem("journey") != undefined) {
  //     this.isStarted = true;
  //     this.startJourney();
  //   } else {
  //     this.isStarted = false;
  //   }
  // }

  /**
   * emargency alerts
   */
  emergency() {
    this.emergencyService.emergency({
      bus_id: window.localStorage['bus_id'],
      route_id: window.localStorage['route_id'],
      user_id: window.localStorage['user_id']
    }).subscribe((data) => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: "Alert send successfully",
        buttons: ['OK']
      });
      alert.present();
    }, (error) => {
      this.showAlert(error.msg);
    })
  }
  /**
   * call the checkMapNative element method 
   */
  ionViewDidLoad() {
    let mapEle = this.mapElement.nativeElement;
    if (!mapEle) {
      console.error('Unable to initialize map, no map element with #map view reference.');
      return;
    } else {
      this.loadMap(mapEle)
    }
  }

  /**
   * load map according to the Drivers current location
   * @param mapEle 
   */
  loadMap(mapEle) {

    let scope = this;
    this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 10000, enableHighAccuracy: true }).then((resp) => {
      scope.currentLocation = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      scope.map = new google.maps.Map(mapEle, {
        zoom: 13,
        center: scope.currentLocation,
        scrollwheel: false,
        styles: mapConfig.styles,
        panControl: false,
        streetViewControl: false,
        mapTypeControl: false,
        fullScreenControl: false
      });
    }).catch((error) => {
      console.log(error);
      this.showAlert("Please check location is on");
    }).then(() => {
      this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay);
    })
  }

  /**
   * drow the route according to the bus driver using way points
   */
  calculateAndDisplayRoute(directionsService, directionsDisplay) {
    let scope = this;
    directionsService.route({
      origin: "Kaduwela",
      destination: "Kollupitiya",
      travelMode: 'DRIVING',
      // waypoints: waypts
    }, function (response, status) {
      if (status === 'OK') {
        directionsDisplay.setDirections(response);
        directionsDisplay.setOptions({ suppressMarkers: true });
        directionsDisplay.setMap(scope.map)
        scope.viewBuses();
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

  /**
   * get active tracker locations from the database and view on the map
   */
  viewBuses() {
    let scope = this;
    this.locations = this.locationDistributor.getLocations().subscribe(data => {
      scope.deleteMarkers();
      console.log(data)
      var t = JSON.parse(JSON.stringify(data))
      console.log(typeof (t.tracker))
      if (Array.isArray(t.tracker)) {
        t.tracker.forEach(element => {
          let updatelocation = new google.maps.LatLng(element.latitude, element.longitude);
          var image = "assets/img/busmarker.png"
          this.addMarker(updatelocation, image);
        })
        this.setMapOnAll(this.map);
      }
    });
  }

  addMarker(location, image) {
    var icon = {
      url: image, // url
      scaledSize: new google.maps.Size(30, 30), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor
    };
    console.log(location);

    // animation: google.maps.Animation.BOUNCE,
    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: icon
    });
    this.markers.push(marker);
  }

  setMapOnAll(map) {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.clearMarkers();
    this.markers = [];
  }

  /**
   * set single marker using input object
   * @param location
   */
  setZoom(location) {
    this.map.setCenter(location);
    this.map.setZoom(12);
  }

  /**
   * start geolation watch and get the cordinates when cordinate changes
   */
  startJourney(startLocation) {
    var a = window.localStorage['allocated_time'];
    var timeParts = a.split(":");
    this.counter = ((+timeParts[0] * (60000 * 60)) + (+timeParts[1] * 60000)) / 1000;
    this.countDown = Observable.timer(0, this.tick)
      .take(this.counter)
      .map(() => --this.counter)
    let scope = this;
    window.localStorage['journey'] = 1;
    this.watch = this.geolocation.watchPosition().subscribe((data) => {
      scope.currentLocation = new google.maps.LatLng(data.coords.latitude, data.coords.longitude);
      scope.setZoom(scope.currentLocation);
      var location = {
        latitude: data.coords.latitude,
        longitude: data.coords.longitude,
        bus: window.localStorage['bus_id'],
        startTime: Date.now(),
        route_id: window.localStorage['route_id'],
        start_location: startLocation
      }
      scope.sendLocation(location);
      // scope.startTimer();
    }, (error) => {
      console.log(error);
    });
  }

  /**
   * finish journey and deactivate the tracker from the server
   */
  finishJourney() {
    let scope = this;
    if (this.watch != null && this.watch != undefined) {
      this.watch.unsubscribe();
    }
    var location = {
      bus: window.localStorage['bus_id'],
      endTime: Date.now(),
      total_km: window.localStorage['total_km'],
      status: "Deactive",
      route_id: window.localStorage['route_id'],
    }
    window.localStorage.removeItem('journey');
    scope.sendLocation(location);
    this.viewBuses();
    this.isStarted = false;
  }

  /**
   * show confirm journey alert
   */
  startJourneyAlert(data) {
    let alert = this.alertCtrl.create({
      title: 'Confirm Journey',
      message: 'Do you want to start journey?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.startJourney(data);
            this.isStarted = true;

          }
        }
      ]
    });
    alert.present();
  }

  /**
   * show finish journey alert
   */
  finishJourneyAlert() {
    if (this.timer != null && this.timer != undefined) {

      console.log("sdasdas")
      this.timer.unsubscribe();
    }

    let alert = this.alertCtrl.create({
      title: 'Confirm Journey',
      message: 'Do you want to finish journey?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.finishJourney();
          }
        }
      ]
    });
    alert.present();
  }

  showAlert(error) {
    const alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: error,
      buttons: ['OK']
    });
    alert.present();
  }

  showRadio() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Select Start Location');

    alert.addInput({
      type: 'radio',
      label: 'Kaduwela',
      value: 'Kaduwela',
      checked: true
    });
    alert.addInput({
      type: 'radio',
      label: 'Koswatta',
      value: 'Koswatta'
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.startJourneyAlert(data)
        console.log(data);
      }
    });
    alert.present();
  }
}

// export class AppComponent implements OnInit {

// }

@Pipe({
  name: 'formatTime'
})
export class FormatTimePipe implements PipeTransform {

  transform(value: number): string {
    const minutes: number = Math.floor(value / 60);
    return ('00' + minutes).slice(-2) + ':' + ('00' + Math.floor(value - minutes * 60)).slice(-2);
  }

}