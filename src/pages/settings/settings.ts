import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { LoginPage } from "../login/login";
import { UserProvider } from '../../providers/user/user';
/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  userDetails: any;
  user_id: any = window.localStorage['user_id'];
  profile = {
    name: "",
    language: "en",
    email: "",
    contact: "",
    password: "abc123",
    newPassword: "abc123",
    confirmPassword: "abc123"
  }

  constructor(public nav: NavController, private user: UserProvider, private alertCtrl: AlertController) {
    user.getUserByID({ user_id: this.user_id }).subscribe((data) => {
      console.log(data)
      this.profile.name = data.data.first_name + ' ' + data.data.last_name;
      this.profile.email = data.data.email;
      this.profile.contact = data.data.contact_no;
    }, (error) => {
      this.showAlert("Ops!!", error.msg)
    })
  }

  showAlert(title, message) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }
  // logout
  logout() {
    this.nav.setRoot(LoginPage);
  }
}
