import { Component, ViewChild } from "@angular/core";
import { Platform, Nav, AlertController } from "ionic-angular";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from "../pages/login/login";
import { HomePage } from "../pages/home/home";
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SettingsPage } from '../pages/settings/settings';
import { UserProvider } from "../providers/user/user";

export interface MenuItem {
  title: string;
  component: any;
  icon: string;
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  email: String;
  name: any = "chamindu";
  user_id: any = window.localStorage['user_id'];
  appMenuItems: Array<MenuItem>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public user: UserProvider,
    private alertCtrl: AlertController
  ) {
    user.getUserByID({ user_id: this.user_id }).subscribe((data) => {
      console.log(data)
      this.name = data.data.first_name + ' ' + data.data.last_name;
      this.email = data.data.email;
    }, (error) => {
      this.showAlert("Ops!!", error.msg)
    })
    this.initializeApp();

    this.appMenuItems = [
      { title: 'Map', component: HomePage, icon: 'navigate' },
      { title: 'Dashboard', component: DashboardPage, icon: 'stats' },
      { title: 'Settings', component: SettingsPage, icon: 'settings' },
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  logout() {
    this.nav.setRoot(LoginPage);
  }
  showAlert(title, message) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }
}

