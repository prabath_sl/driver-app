import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';

import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage, FormatTimePipe } from '../pages/home/home';
import { LoginPage } from "../pages/login/login";
import { SettingsPage } from '../pages/settings/settings';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { UserProvider } from '../providers/user/user';
import { MainProvider } from '../providers/main/main';
import { JwtProvider } from '../providers/jwt/jwt';
import { LocationDistributorProvider } from '../providers/location-distributor/location-distributor';
import { BusProvider } from '../providers/bus/bus';
import { LocationProvider } from '../providers/location/location';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SettingsPage,
    FormatTimePipe,
    DashboardPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SettingsPage,
    DashboardPage,
    LoginPage,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UserProvider,
    MainProvider,
    LocationProvider,
    JwtProvider,
    LocationDistributorProvider,
    BusProvider,
  ]
})
export class AppModule { }
